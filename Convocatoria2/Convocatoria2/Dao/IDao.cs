﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convocatoria2.Dao
{
    interface IDao <T>
    {
        void save(T t);
        void ubdate(T t);
        void delete(T t);
        List<T> findAll();

    }
}
