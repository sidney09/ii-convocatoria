﻿using Convocatoria2.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convocatoria2.Dao
{
    interface IDaoExtinguidor : IDao<Extinguidores>
    {
        Extinguidores findById(string Id);
        List<Extinguidores> findByMarca(string Marca);
        Extinguidores findByTipo(string Tipo_extin);
    }
}
