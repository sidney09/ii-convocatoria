﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convocatoria2.entities
{
    class Extinguidores
    {
        private int id;
        private string categoria;
        private string marca;
        private string capacidad;
        private string tipo_extin;
        private string unid_medida;
        private int cantidad;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Categoria
        {
            get
            {
                return categoria;
            }

            set
            {
                categoria = value;
            }
        }

        public string Marca
        {
            get
            {
                return marca;
            }

            set
            {
                marca = value;
            }
        }

        public string Capacidad
        {
            get
            {
                return capacidad;
            }

            set
            {
                capacidad = value;
            }
        }

        public string Tipo_extin
        {
            get
            {
                return tipo_extin;
            }

            set
            {
                tipo_extin = value;
            }
        }

        public string Unid_medida
        {
            get
            {
                return unid_medida;
            }

            set
            {
                unid_medida = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public Extinguidores(int id, string categoria, string marca, string capacidad, string tipo_extin, string unid_medida, int cantidad)
        {
            this.Id = id;
            this.Categoria = categoria;
            this.Marca = marca;
            this.Capacidad = capacidad;
            this.Tipo_extin = tipo_extin;
            this.Unid_medida = unid_medida;
            this.Cantidad = cantidad;
        }

        public Extinguidores()
        {

        }
    }
}
