﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convocatoria2.util
{
    class RandomFileBinarySearch
    {
        public static BinaryReader brhExtinguidor;
        public static int SIZE;

        public static int runBinarySearchRecursively(int key, int low, int high)
        {
            if (brhExtinguidor == null)
            {
                return -1;
            }
            int middle = (low + high) / 2;

            if (high < low)
            {
                return -1;
            }

            long pos = 8 + SIZE * middle;
            brhExtinguidor.BaseStream.Seek(pos, SeekOrigin.Begin);
            int id = brhExtinguidor.ReadInt32();

            if (key == id)
            {
                return middle;
            }
            else if (key < id)
            {
                return runBinarySearchRecursively(
                        key, low, middle - 1);
            }
            else
            {
                return runBinarySearchRecursively(
                        key, middle + 1, high);
            }
        }
    }
}
